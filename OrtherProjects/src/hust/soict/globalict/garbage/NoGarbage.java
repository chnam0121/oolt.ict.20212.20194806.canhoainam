package hust.soict.globalict.garbage;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.Scanner;

public class NoGarbage {
	public static void main(String[] args) {
		StringBuffer data=new StringBuffer() ;
		String url ="/home/namhoai/dict-million-database.txt";
		try {
		File garbageFile = new File(url);
		Scanner garbageReader = new Scanner(garbageFile);
		while(garbageReader.hasNextLine()) {
			data.append(garbageReader.nextLine());
		}
		//String string= data.toString();
		System.out.println("Done!");
		garbageReader.close();
		}catch (FileNotFoundException e) {
		      System.out.println("An error occurred.");
		      e.printStackTrace();
		 }
		}
}
