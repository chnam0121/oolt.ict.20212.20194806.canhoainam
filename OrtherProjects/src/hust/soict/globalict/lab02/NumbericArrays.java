package hust.soict.globalict.lab02;
import java.util.Scanner;
class NumbericArraysUtils {
		public static void sort(int[] arr) {
			int i,j;
			for(i=0;i<arr.length;i++) {
				for(j=i+1;j<arr.length;j++) {
					if(arr[j]<arr[i]) {
						arr[i]=arr[i]+arr[j];
						arr[j]=arr[i]-arr[j];
						arr[i]=arr[i]-arr[j];
					}
				}
			}
		}
	    public static void sort(double[] arr) {
	    	int i,j;
			for(i=0;i<arr.length;i++) {
				for(j=i+1;j<arr.length;j++) {
					if(arr[j]<arr[i]) {
						arr[i]=arr[i]+arr[j];
						arr[j]=arr[i]-arr[j];
						arr[i]=arr[i]-arr[j];
					}
				}
			}
		}
	    public static int sum(int[] arr) {
	    	int sum=0;
	    	for(int i=0;i<arr.length;i++) {
	    		sum+=arr[i];
	    	}
	    	return sum;
	    }
	    public static double sum(double[] arr) {
	    	double sum=0.0;
	    	for(int i=0;i<arr.length;i++) {
	    		sum+=arr[i];
	    	}
	    	return sum;
	    }
	    public static double average(int[] arr) {
	    	return (double)sum(arr)/(double)arr.length;
	    }
	    public static double average(double[] arr) {
	    	return sum(arr)/(double)arr.length;
	    }
}

public class NumbericArrays {

	public static void main(String[] args) {
		Scanner sc=new Scanner(System.in);
		int n;
		System.out.print("Enter number of elements: ");
		n=sc.nextInt();
		int i;
		int[] a=new int[n];
		for(i=0;i<n;i++) {
			System.out.print("Enter element "+i+": ");
			a[i]=sc.nextInt();
		}
		System.out.println("\nSum of array: "+NumbericArraysUtils.sum(a));
		System.out.printf("Average of array: %.3f\n",NumbericArraysUtils.average(a));
		System.out.println("Array after sorting: ");
		NumbericArraysUtils.sort(a);
		for(i=0;i<a.length;i++) {
			System.out.print(a[i]+" ");
		}
		System.out.println("");
	}

}
