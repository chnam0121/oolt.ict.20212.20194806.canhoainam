package hust.soict.globalict.lab02;

import java.util.Scanner;
public class FindDaysOfMonth {
	static String[] month= new String[] {"january","february","march","april","may","june","july","august","september","october","november","december"};
	static String[] inNumber = new String[] {"1","2","3","4","5","6","7","8","9","10","11","12"};
	public static int monthCheck(String strMonth) {
		String m=strMonth.toLowerCase();
		int i;
		if(m.length()==4&& m.charAt(m.length()-1)=='.') m=m.substring(0,3);
		if(m.length()<=2) {
			for(i=0;i<12;i++) {
				if(m.equals(inNumber[i])) return i+1;
			}
		}else if(m.length()<=3) {
			for(i=0;i<12;i++) {
				if(m.equals(month[i].substring(0,3))) return i+1;
			}
		}else {
			for(i=0;i<12;i++) {
				if(m.equals(month[i])) return i+1;
			}
		}
		return 0;
	}
	public static void main(String[] args) {
		String strMonth;
		int iYear;
		int iMonth;
		Scanner sc=new Scanner(System.in);
		do {
			System.out.print("Enter month: ");
			strMonth=sc.next();
			iMonth=monthCheck(strMonth);
			if(iMonth==0) System.out.println("Error! Please enter again!\n(The month has to be full name, abbreviation, in 3 letters, or in number)");
		}while(iMonth==0);
		do {
			System.out.print("Enter year: ");
			iYear=sc.nextInt();
			if(iYear<0) System.out.println("Error! Please enter again!\n(The year has to be higher than 0)");
		}while(iYear<0);
		int day=0;
		switch(iMonth) {
		case 1:
		case 3:
		case 5:
		case 7:
		case 8:
		case 10:
		case 12:
			day=31;
			break;
		case 4:
		case 6:
		case 9:
		case 11:
			day =30;
			break;
		case 2:
			if(iYear%4!=0 || (iYear%100==0 && iYear%400 !=0)) day=28;
			else day=29;
		}
		System.out.println("The "+month[iMonth-1]+" of "+iYear+" has "+day+" days.");
	}
}
