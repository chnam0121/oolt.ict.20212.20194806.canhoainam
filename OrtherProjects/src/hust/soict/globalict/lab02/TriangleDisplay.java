package hust.soict.globalict.lab02;

import java.util.Scanner;
public class TriangleDisplay {

	public static void main(String[] args) {
		Scanner sc=new Scanner(System.in);
		System.out.print("Enter height of triangle: ");
		int h=sc.nextInt();
		int i,j;
		int space=h-1;
		for(i=1;i<=h;i++) {
			for(j=1;j<=space;j++) {
				System.out.print(" ");
			}
			for(j=1;j<=2*i-1;j++) {
				System.out.print("*");
			}
			space--;
			System.out.println("");
		}
	}

}
