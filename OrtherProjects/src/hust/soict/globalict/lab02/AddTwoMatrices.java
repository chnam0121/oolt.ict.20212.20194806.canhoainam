package hust.soict.globalict.lab02;

public class AddTwoMatrices {
	public static void main(String[] args) {
		int[][] a= {
				{2,1},{0,3},{1,9},{9,9}
		};
		int[][] b= {
				{1,2},{1,0},{2,0},{0,1}
		};
		if(a.length!=b.length || a[0].length!=b[0].length) {
			System.out.println("Error");
		}
		int[][] c= new int[a.length][a[0].length];
		int i,j;
		System.out.println("Sum of 2 matrices: ");
		for(i=0;i<a.length;i++) {
			for(j=0;j<a[0].length;j++) {
				c[i][j]=a[i][j]+b[i][j];
				System.out.print(c[i][j]+" ");
			}
			System.out.println("");
		}
		
	}
}
