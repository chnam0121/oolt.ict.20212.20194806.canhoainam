package hust.soict.globalict.lab01;

import java.util.Scanner;

public class EquationSolve {
    public static void menu() {
        System.out.println(" _______________________________MENU_____________________________");
        System.out.println("|1. Solve the first-degree equation with one variable            |");
        System.out.println("|2. Solve the system of first-degree equations with two variables|");
        System.out.println("|3. Solve the second-degree equation with one variable           |");
        System.out.println("|4. Exit                                                         |");
        System.out.println("|________________________________________________________________|");
    }

    public static void linearEquation() {
        System.out.println("___General form: ax + b___");
        System.out.print("- Enter a: ");
        Scanner sc = new Scanner(System.in);
        double a = sc.nextDouble();
        System.out.print("- Enter b: ");
        double b = sc.nextDouble();
        if (a == 0) {
            if (b == 0)
                System.out.println("Equation has infintely many solutions.");
            else
                System.out.println("Equation has no solution.");
            return;
        }
        String result = String.format("%.3g", -b / a);
        System.out.println("Solution of the equation: " + result);
    }

    public static void linearSystem() {
        Scanner sc = new Scanner(System.in);
        double a1, b1, c1, a2, b2, c2;
        System.out.println("___General form: a1x + b1y = c1 | a2x + b2y = c2___");
        System.out.print("- Enter a1: ");
        a1 = sc.nextDouble();
        System.out.print("- Enter b1: ");
        b1 = sc.nextDouble();
        System.out.print("- Enter c1: ");
        c1 = sc.nextDouble();
        System.out.print("___\n- Enter a2: ");
        a2 = sc.nextDouble();
        System.out.print("- Enter b2: ");
        b2 = sc.nextDouble();
        System.out.print("- Enter c2: ");
        c2 = sc.nextDouble();
        double D = b2 * a1 - b1 * a2;
        double D1 = c1 * b2 - c2 * b1;
        double D2 = c2 * a1 - c1 * a2;
        if (D != 0) {
            System.out.printf("System equation has unique solution ( %.3g,%.3g )\n", D1 / D, D2 / D);
        } else if (D == 0) {
            if (D1 == D2 && D1 == 0) {
                System.out.println("System equation has infinitely many solutions");
            } else {
                System.out.println("System equation has no solution");
            }
        }
    }

    public static void quadraticEquation() {
        Scanner sc = new Scanner(System.in);
        System.out.println("___General form ax^2 + bx +c___");
        double a, b, c;
        System.out.print("Enter a: ");
        a = sc.nextDouble();
        System.out.print("Enter b: ");
        b = sc.nextDouble();
        System.out.print("Enter c: ");
        c = sc.nextDouble();
        if (a == 0) {
            if (b == 0) {
                if (c == 0)
                    System.out.println("Equation has infintely many solutions.");
                else
                    System.out.println("Equation has no solution.");
            } else {
                String result = String.format("%.3g", -c / b);
                System.out.println("Solution of the equation: " + result);
            }
        } else {
            double delta = b * b - 4 * a * c;
            if (delta == 0) {
                System.out.printf("The equation has double root: %.3g\n", -b / (2 * a));
            } else if (delta > 0) {
                System.out.printf("The equation has 2 distinct roots: %.3g and %.3g\n", (-b + delta) / (2 * a),
                        (-b - delta) / (2 * a));
            } else {
                System.out.println("The equation has no solution");
            }
        }
    }

    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        int choice;
        do {
            menu();
            System.out.print("Your choice: ");
            choice = sc.nextInt();
            switch (choice) {
                case 1:
                    linearEquation();
                    break;
                case 2:
                    linearSystem();
                    break;
                case 3:
                    quadraticEquation();
                    break;
                case 4:
                    break;
                default:
                    System.out.println("Error! Please retype!");
            }
        } while (choice != 4);
        sc.close();
    }
}
