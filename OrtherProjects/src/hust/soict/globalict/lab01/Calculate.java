package hust.soict.globalict.lab01;

import javax.swing.JOptionPane;

public class Calculate {
        public static void main(String[] args) {
                String strNum1, strNum2;
                String strNotification;
                strNum1 = JOptionPane.showInputDialog(null, "Please enter the first number: ", "Calculator-Input",
                                JOptionPane.INFORMATION_MESSAGE);
                strNum2 = JOptionPane.showInputDialog(null, "Please enter the second number: ", "Calculator-Input",
                                JOptionPane.INFORMATION_MESSAGE);
                double num1 = Double.parseDouble(strNum1);
                double num2 = Double.parseDouble(strNum2);
                String cal;
                cal = String.format("%g", num1 + num2);
                // System.out.println(cal);
                strNotification = ("- Sum: " + cal);
                cal = String.format("%g", num1 - num2);
                strNotification += ("\n- Difference: " + cal);
                cal = String.format("%g", num1 * num2);
                strNotification += ("\n- Product: " + cal);
                cal = String.format("%g", num1 / num2);
                strNotification += ("\n- Quotient: " + cal);
                JOptionPane.showMessageDialog(null, strNotification, "Calculator-Output",
                                JOptionPane.INFORMATION_MESSAGE);
                System.exit(0);
        }
}
