package hust.soict.globalict.aims;
import hust.soict.globalict.aims.media.DigitalVideoDisc;
import hust.soict.globalict.aims.media.Media;
import hust.soict.globalict.aims.order.Order;
public class DiskTest {
	public static void main(String[] args) {
		//Test boolean search(String title) method
        DigitalVideoDisc dvd = new DigitalVideoDisc("DVD01","Sherlock Holmes");
        String title = "Holmes.Sherlock!";
        System.out.print("Test search method with \"Sherlock Holmes\" and \"Holmes.Sherlock!\": ");
        if (dvd.search(title)==true)
            System.out.println("Found!");
        else
            System.out.println("Noy found yet!");
        //-> The output is: Found!
        
        //Test method DigitalVideoDisc getALuckyItem()
        Order myOrder = Order.createOrder();
        Media dvd1 = new DigitalVideoDisc("The Lion King","Animation","Roger Allers",87,19.95f);
		myOrder.addMedia(dvd1);
		
		Media dvd2 = new DigitalVideoDisc("Star Wars","Science Fiction","George Lucas",124,24.95f);
		myOrder.addMedia(dvd2);
		
		DigitalVideoDisc dvd3 = new DigitalVideoDisc("Aladdin","Animation","John Musker",90,18.99f);
		myOrder.addMedia(dvd3);
        
		Media luckyItem= myOrder.getALuckyItem();
		myOrder.showOrderInformation();
        
    }
}
