package hust.soict.globalict.aims;
import hust.soict.globalict.aims.order.Order;
import hust.soict.globalict.aims.utils.MyDate;
import hust.soict.globalict.aims.media.*;
import hust.soict.globalict.aims.media.factory.*;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class Aims {
	public static void showUserMenu() {
			System.out.println("Welcome to AIMS Store: ");
			System.out.println("--------------------------------");
			System.out.println("1. Create new order");
			System.out.println("2. Search for an item from the list by title");
			System.out.println("3. Add item to order by id (id in the list of available items of the store");
			System.out.println("4. Remove item from order by id (id in the order)");
			System.out.println("5. Display the order information");
			System.out.println("0. Exit");
			System.out.println("--------------------------------");
			System.out.println("Please choose a number: 0-1-2-3-4-5");
	}
	public static void showMenu() {
		System.out.println(" __________AIMS_SYSTEM___________");
		System.out.println("|1. Access as Admin              |");
		System.out.println("|2. Access as User               |");
		System.out.println("|0. Exit program.                |");
		System.out.println("|________________________________|");
	}
	public static void createNewItem(List<Media> mediaItems) {
		Scanner sc = new Scanner(System.in);
		System.out.println(" 1 Create a DVD");
		System.out.println(" 2 Create a CD");
		System.out.println(" 3 Create a book");
		System.out.println("-> Please choose a number: 1-2-3");
		int type;
		System.out.print("- Your choice: ");
		type=sc.nextInt();
		switch (type) {
		case 1:
			mediaItems.add(createMedia(new DVDCreation()));
			System.out.println("Successfull!");
			break;
		case 2:
			mediaItems.add(createMedia(new CDCreation()));
			System.out.println("Successfull!");
			break;
		case 3:
			mediaItems.add(createMedia(new BookCreation()));
			System.out.println("Successfull!");
			break;
		default:
			System.out.println("Error!");
			break;
		}
		
	}
	public static void deleteItem(List<Media> mediaItems,int id) {
		if(id<0||id>=mediaItems.size()) {System.out.println("Error!");return;}
		mediaItems.remove(id);
		System.out.println("Successfull!");
	}
	public static void displayItemList(List<Media> mediaItems){
		for(int i=0;i<mediaItems.size();i++) {
			System.out.print((i)+". ");
			System.out.println(mediaItems.get(i));
		}
	}
	public static void showAdminMenu() {
		System.out.println("Product Management Application: ");
		System.out.println("--------------------------------");
		System.out.println("1. Create new item");
		System.out.println("2. Delete item by id");
		System.out.println("3. Display the items list");
		System.out.println("0. Exit");
		System.out.println("--------------------------------");
		System.out.println("Please choose a number: 0-1-2-3");
		}
	public static Media createMedia(MediaCreation mc) {
		return mc.createMediaFromConsole();
		}
	public static void Admin(List<Media> mediaItems) {
		int choice,id;
		Scanner sc = new Scanner(System.in);
			do {
			showAdminMenu();
			System.out.print("- Your choice: ");
			choice = sc.nextInt();
			switch (choice) {
			case 1:
				createNewItem(mediaItems);
				break;
			case 2:
				System.out.print("Enter Id of the Item: ");
				id = sc.nextInt();
				deleteItem(mediaItems, id);
				break;
			case 3:
				displayItemList(mediaItems);
				break;
			case 0:
				break;
			default:
				System.out.println("Error!");
				break;
			}
		    }while(choice!=0);
	}
	public static void User(List<Media> mediaItems) {
		int choice;
		Scanner sc = new Scanner(System.in);
		Order myOrder=null,newOrder=null;
		Media mediaItem;
		int i,id;
		//,kind;char ans;
		String title;
		MyDate currentDate;
		do {
			showUserMenu();
			System.out.print("- Your choice: ");
			choice = sc.nextInt();
			switch (choice) {
			case 1:
				newOrder= Order.createOrder();
				if(newOrder!=null) {
					currentDate=new MyDate();
					currentDate.accept();
					newOrder.setDateOrdered(currentDate);
					myOrder = newOrder;
					System.out.println("Successful!");
					}
				break;
			case 2:
				sc.nextLine();
				System.out.print("Enter the title: ");
				title = sc.nextLine();
				for(i=0;i<mediaItems.size();i++) {
					if(title.equals(mediaItems.get(i).getTitle())) {
						System.out.print("["+i+"] ");
						System.out.println(mediaItems.get(i));
					}
				}
				break;
			case 3:
				/*System.out.print("Enter kind of Media(Enter 1 for DVD, 2 for CD, 3 for Book): ");
				kind = sc.nextInt();*/
				System.out.print("Enter the id: ");
				id = sc.nextInt();
				if(id<0 || id >=mediaItems.size()) {System.out.println("Error");break;}
				mediaItem = mediaItems.get(id);
				myOrder.addMedia(mediaItem);
				/*if(kind == Order.MEDIA_CD || kind == Order.MEDIA_DVD) {
				System.out.print("Would you want to play this media(y or n)?: ");
				ans = sc.next().charAt(0);
				if(ans == 'y') {
					System.out.println("-----------------------------------------");
					if(kind == Order.MEDIA_DVD)
					((DigitalVideoDisc)mediaItem).play();
					else 
						((CompactDisc)mediaItem).play();
					System.out.println("-----------------------------------------");
				}
				}*/
				break;
			case 4:
				System.out.print("- Enter ID of the item: ");
				id=sc.nextInt();
				myOrder.removeMedia(id);
				break;
			case 5:
				myOrder.showOrderInformation();
				break;
			case 0:
				break;
			default:
				System.out.println("Error!");
				break;
			}
			}while(choice!=0);
	}
	public static void main(String[] args) {
		List<Media> mediaItems = new ArrayList<Media>();
		int choice;
		Scanner sc = new Scanner(System.in); 
		do {
			showMenu();
			System.out.print("- Your choice: ");
			choice = sc.nextInt();
			switch (choice) {
			case 1:
				Admin(mediaItems);
			break;
			case 2:
				User(mediaItems);
		    break;
			case 0:
				sc.close();
				break;
			default:
				System.out.println("Error");
				break;
			}
	    }while(choice!=0);
}
}
