package hust.soict.globalict.aims.media;

public abstract class Media implements Comparable<Media>{
	private String title;
	private String category;
	private float cost;
	private int id;
	public Media() {
	}
	public Media(String title){
		this.title=title;
	}
	public Media(String title, String category) {
		
		this.title = title;
		this.category = category;
	}
	public Media(String title, String category, float cost) {
		
		this.title = title;
		this.category = category;
		this.cost = cost;
	}
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id=id;
	}
	public String getTitle() {
		return title;
	}
	public String getCategory() {
		return category;
	}
	public float getCost() {
		return cost;
	}
	
	public String toString() {
		return " [Title: "+title+"] [Category: "+category+"]";
	}
	@Override
	public boolean equals(Object o) {
		if(o instanceof Media) {
			if(((Media) o).getId()==this.id) return true;
		}
		return false;
	}
	@Override
	public int compareTo(Media obj) {
		return this.getTitle().compareTo(obj.getTitle());
	}
}