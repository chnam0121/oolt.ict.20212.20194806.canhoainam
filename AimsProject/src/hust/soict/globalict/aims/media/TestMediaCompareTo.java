package hust.soict.globalict.aims.media;
import java.util.List;
import java.util.Scanner;
import java.util.ArrayList;
import hust.soict.globalict.aims.media.factory.DVDCreation;
public class TestMediaCompareTo {
	public static void showMenu() {
		System.out.println("-----------------MENU-----------------");
		System.out.println("1. Add a DVD");
		System.out.println("2. Print DVDs currently in the order");
		System.out.println("3. Sort the DVDs in the Order");
		System.out.println("0. Exit");
		System.out.println("--------------------------------------");
	}
	public static void main(String[] args) {
		int choice;
		List<DigitalVideoDisc> discs = new ArrayList<DigitalVideoDisc>();
		java.util.Iterator<DigitalVideoDisc> iterator;
		DigitalVideoDisc dvd;
		Scanner sc = new Scanner(System.in);
		do {
			showMenu();
			System.out.print("-> Your choice: ");
			choice=sc.nextInt();
			switch (choice) {
			case 1:
				dvd = (DigitalVideoDisc)(new DVDCreation().createMediaFromConsole());
				discs.add(dvd);
				break;
			case 2:
				iterator= discs.iterator();
				System.out.println("----------------------------------");
				System.out.println("The DVDs currently in the order are: ");
				while(iterator.hasNext()) {
					System.out.println(((DigitalVideoDisc)iterator.next()).getTitle());
				}
				break;
			case 3:
				java.util.Collections.sort(discs);
				iterator = discs.iterator();
				System.out.println("----------------------------------");
				System.out.println("The DVDs in sorted order are: ");
				while(iterator.hasNext()) {
					System.out.println(((DigitalVideoDisc)iterator.next()).getTitle());
				}
				break;
			case 0:
				break;
			default:
				System.out.println("Error!");
				break;
			}
	}while(choice!=0);
}
}
