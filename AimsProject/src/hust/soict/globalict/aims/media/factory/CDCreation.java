package hust.soict.globalict.aims.media.factory;

import java.util.Scanner;

import hust.soict.globalict.aims.media.CompactDisc;
import hust.soict.globalict.aims.media.Media;

public class CDCreation implements MediaCreation{
	@Override
	public Media createMediaFromConsole() {
			Scanner sc = new Scanner(System.in);
			String title, category,artist ,director;
			float cost;
			int n;
			System.out.print("- Please enter cd title: ");
			title= sc.nextLine();
			System.out.print("- Please enter cd category: ");
			category = sc.nextLine();
			System.out.print("- Please enter artist: ");
			artist=sc.nextLine();
			System.out.print("- Please enter director: ");
			director=sc.nextLine();
			System.out.print("- Please enter cd price: ");
			cost=sc.nextFloat();
			CompactDisc cd = new CompactDisc(title, category, artist, director, cost);
			System.out.println("ENTER CD TRACK LIST: ");
			System.out.print("- Please enter number of tracks: ");
			n=sc.nextInt();
			System.out.println("- Please enter the information of each track: ");
			for(int i=0;i<n;i++) {
				System.out.println("Track "+(i+1));
				cd.addTrack(TrackCreation.createTrackFromConsole());
			}
			return cd;
	}

}
