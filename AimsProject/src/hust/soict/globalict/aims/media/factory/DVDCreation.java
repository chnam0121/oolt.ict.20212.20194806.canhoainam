package hust.soict.globalict.aims.media.factory;
import java.util.Scanner;
import hust.soict.globalict.aims.media.DigitalVideoDisc;
import hust.soict.globalict.aims.media.Media;
public class DVDCreation implements MediaCreation{
	@Override
	public Media createMediaFromConsole() {
		String title, category, director;
		float length;
		float cost;
		Scanner sc = new Scanner(System.in);
		System.out.print("- Please enter dvd title: ");
		title= sc.nextLine();
		System.out.print("- Please enter dvd category: ");
		category = sc.nextLine();
		System.out.print("- Please enter dvd director: ");
		director=sc.nextLine();
		System.out.print("- Please enter dvd length: ");
		length=sc.nextFloat();
		System.out.print("- Please enter dvd price: ");
		cost=sc.nextFloat();
		return new DigitalVideoDisc(title, category, director, length, cost);
	}
}
