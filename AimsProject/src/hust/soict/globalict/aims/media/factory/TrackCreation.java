package hust.soict.globalict.aims.media.factory;
import java.util.Scanner;
import hust.soict.globalict.aims.media.Track;
public class TrackCreation {
	public static Track createTrackFromConsole() {
		Scanner sc = new Scanner(System.in);
		System.out.print("- Track Title: ");
		String titleTrack=sc.nextLine();
		System.out.print("- Track Length: ");
		float lengthTrack=sc.nextFloat();
		return new Track(titleTrack, lengthTrack);
	}
}
