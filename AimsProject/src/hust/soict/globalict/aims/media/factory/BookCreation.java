package hust.soict.globalict.aims.media.factory;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;
import hust.soict.globalict.aims.media.Book;
import hust.soict.globalict.aims.media.Media;
public class BookCreation implements MediaCreation{
	@Override
	public Media createMediaFromConsole() {
		String title,category;
		float cost;
		List<String> authorsList = new ArrayList<String>();
		Scanner sc = new Scanner(System.in);
		System.out.print("- Please enter book title: ");
		title= sc.nextLine();
		System.out.print("- Please enter book category: ");
		category = sc.nextLine();
		System.out.print("- Please enter book cost: ");
		cost=sc.nextFloat();
		String authorList;
		sc.nextLine();
		System.out.println("ENTER BOOK AUTHORS (Authors' names are separated by commas): ");
		authorList = sc.nextLine();
		String parts[] = authorList.split(",");
		for(int i=0;i<parts.length;i++) {
			authorsList.add(parts[i]);
		}
		Book.checkAuthors(authorsList);
		return new Book(title, category, cost, authorsList);
	}
}
