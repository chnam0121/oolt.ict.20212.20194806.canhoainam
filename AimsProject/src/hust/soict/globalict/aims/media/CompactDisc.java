package hust.soict.globalict.aims.media;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class CompactDisc extends Disc implements Playable{
	private String artist;
	private List<Track> tracks = new ArrayList<Track>();
	public CompactDisc() {
		super();
	}
	
	public CompactDisc(String title, String category) {
		super(title, category);
	}

	public CompactDisc(String title) {
		super(title);
	}

	public CompactDisc(String title, String category, float cost) {
		super(title,category,cost);
	}
	public CompactDisc(String title, String category,String artist,String director, float cost) {
		super(title,category,director,cost);
		this.artist = artist;
	}
	public CompactDisc(String title, String category, String artist, String director,float length, List<Track> tracks, float cost) {
		super(title,category,director,length,cost);
		this.artist = artist;
		this.tracks = tracks;
	}
	public String getArtist() {
		return artist;
	}
	
	public int getNumOftracks() {
		return tracks.size();
	}
	
	public float getLength() {
		float sum=0;
		for (int i=0;i<tracks.size();i++){
			sum+=tracks.get(i).getLength();
		}
		return sum;
	}
	public void addTrack(Track track) {
			if(tracks.contains(track)) {
				System.out.println("Track "+track.getTitle()+ " already exists");
				return;
			}
		tracks.add(track);
		
	}
	public void removeTrack(Track track) {
		for(int i=0;i<tracks.size();i++) {
			if(tracks.get(i).equals(track)) {
				tracks.remove(i);
				return;
			}
		}
		System.out.println("Track "+track.getTitle()+ " doesn't exist");
		
	}
	public void play() {
		int i;
		System.out.println("Playing CD: " + this.getTitle());
		System.out.println("Total length: "+getLength());
		for(i=0;i<tracks.size();i++) {
			tracks.get(i).play();
		}
	}
	public String toString() {
		return "CD "+super.toString()+" [Artist: "+artist+"] [Total_Length: "+this.getLength()+"] [Cost: "+this.getCost()+"]";
	}
	public int compareTo(Media obj) {
		int numOfTracks=((CompactDisc)obj).getNumOftracks();
		if(tracks.size()>numOfTracks) return 1;
		if(tracks.size()==numOfTracks) {
			float length_t = ((CompactDisc)obj).getLength();
			float length = this.getLength();
			if(length>length_t) return 1;
			if(length==length_t) return 0;
			return -1;
		}
		return -1;
	}
}
