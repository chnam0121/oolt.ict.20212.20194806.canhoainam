package hust.soict.globalict.aims.media;

public class Disc extends Media{
	private String director;
	private float length;
	public String getDirector() {
		return director;
	}
	public float getLength() {
		return length;
	}
	/*public float getCost() {
		return super.getCost();
	}*/
	public Disc(){
		super();
	}
	public Disc(String title) {
		super(title);
	}
	public Disc( String title,String category) {
		super(title,category);
	}
	public Disc( String title,String category,float cost) {
		super(title,category,cost);
	}
	public Disc( String title,String category,String director,float cost) {
		super(title,category,cost);
		this.director=director;
	}
	public Disc( String title,String category,String director) {
		super(title,category);
		this.director=director;
	}
	
	public Disc( String title, String category, String director, float length, float cost) {
		super(title, category, cost);
		this.director = director;
		this.length = length;
	}
	public String toString() {
		return super.toString()+" [Director: "+director+"]";
	}
	
}
