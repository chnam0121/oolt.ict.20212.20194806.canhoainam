package hust.soict.globalict.aims.media;

public class Track implements Playable,Comparable<Track>{
	private String title;
	private float length;
	
	public Track(String title, float length) {
		super();
		this.title = title;
		this.length = length;
	}
	public String getTitle() {
		return title;
	}
	/*public void setTitle(String title) {
		this.title = title;
	}*/
	public float getLength() {
		return length;
	}
	/*public void setLength(float length) {
		this.length = length;
	}*/
	public boolean equals(Track track) {
		if(!this.title.equals(track.getTitle())) return false;
		if(this.length != track.getLength()) return false;
		return true;
	}
	public void play() {
		System.out.println("- Playing Track: " + this.getTitle());
		System.out.println("  Track length: " + this.getLength());
		}
	@Override
	public boolean equals(Object o) {
		if(o instanceof Track) {
			if(((Track) o).getTitle()==this.title && ((Track) o).getLength()==this.length) return true;
		}
		return false;
	}
	@Override
	public int compareTo(Track obj) {
		return title.compareTo(obj.getTitle());
	}
}
