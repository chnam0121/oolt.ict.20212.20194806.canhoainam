package hust.soict.globalict.aims.media;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;


public class Book extends Media{
	List<String> authors = new ArrayList<String>();
	String content;
	List<String> contentTokens;
	Map<String, Integer> wordFrequency;
	public Book() {
		super();
	}
	public Book( String title) {
		super(title);
	}
	public Book( String title,String category) {
		super(title, category);
	}
	public List<String> getAuthors() {
		return authors;
	}
	public void setContent(String content) {
		this.content=content;
		processContent();
	}
	public Book( String title, String category, float cost,List<String> authors) {
		super(title, category, cost);
		checkAuthors(authors);
		this.authors = authors;
	}
	public void addAuthor(String author) {
			if(authors.contains(author)) {
				System.out.println("Author "+author+ " already exists!");
				return;
			}
		authors.add(author);
	}
	public void removeAuthor(String author) {
		for(int i=0;i<authors.size();i++) {
			if(author.equals(authors.get(i))) {
				authors.remove(i);
				break;
			}
		}
	}
	public void updateAuthor(String a,String b) {
		for(int i=0;i<authors.size();i++) {
			if(b.equals(authors.get(i))) {
				System.out.println("Author "+b+ " already exists!");
				return;
			}
		}
		for(int i=0;i<authors.size();i++) {
			if(a.equals(authors.get(i))) {
				authors.set(i, b);
				break;
			}
		}
	}
	public void sortAuthors() {
		Collections.sort(authors);
	}
	
	public String toString() {
		return "Book "+super.toString()+" [Author: "+authors.toString()+"] [Cost: "+super.getCost()+"]\n[The content length: "+contentTokens.size()+"]\n[The tokens list: "+contentTokens.toString()+"]\n[The word Frequency: "+wordFrequency.toString()+"]";
	}
	
	public int compareTo(Media obj) {
		return this.getTitle().compareTo(((Book)obj).getTitle());
	}
	public void processContent() {
		contentTokens=new ArrayList<String>();
		wordFrequency = new TreeMap<String, Integer>();
		String lowerContent = content.toLowerCase();
		contentTokens= Arrays.asList(lowerContent.split(" |\\. |\\."));
		Collections.sort(contentTokens);
		int count;
		for(int i=0;i<contentTokens.size();i++) {
			count =1;
			while(i<contentTokens.size()-1 && contentTokens.get(i).equals(contentTokens.get(i+1))) {i++;count++;}
			wordFrequency.put(contentTokens.get(i), count);
		}
	}
	public static void checkAuthors(List<String> authorList) {
		int j;
		for(int i=0;i<authorList.size();i++) {
			for(j=i+1;j<authorList.size();j++) {
				if(authorList.get(i).equals(authorList.get(j))) {authorList.remove(j);j--;}
			}
		}
	}
	
}
