package hust.soict.globalict.aims.media;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Scanner;
public class DigitalVideoDisc extends Disc implements Playable{
	public DigitalVideoDisc(){
		super();
	}
	public DigitalVideoDisc(String title) {
		super(title);
	}
	public DigitalVideoDisc( String title,String category) {
		super(title,category);
	}
	public DigitalVideoDisc( String title,String category,String director) {
		super(title,category,director); 
	}
	
	public DigitalVideoDisc( String title, String category, String director, float length, float cost) {
		super(title, category,director,length,cost);
	}
	public String toString() {
		return "DVD "+super.toString()+" [Length: "+this.getLength()+"] [Cost: "+this.getCost()+"]";
	}
	public void play() {
		System.out.println("Playing DVD: " + this.getTitle());
		System.out.println("DVD length: " + this.getLength());
	}
	public int compareTo(Media obj) {
		float cost = ((DigitalVideoDisc)obj).getCost();
		if(this.getCost()>cost) return 1;
		if(this.getCost()==cost) return 0;
		return -1;
	}
	boolean check(char x) {
		if(x>='a' && x<='z') return true;
		if(x>='0' && x<='9') return true;
		if(x>='A' && x<='Z') return true;
		return false;
	}
	ArrayList<String> convertToTokens(String tmpTitle){
		ArrayList<String> token = new ArrayList<String>();
		int i=0,j;
		while(i<tmpTitle.length()) {
			while(i<tmpTitle.length() && check(tmpTitle.charAt(i))==false ) i++;
			if(i==tmpTitle.length()) break;
			j=i;
			while(i<tmpTitle.length() && check(tmpTitle.charAt(i))==true ) i++;
			token.add(tmpTitle.substring(j, i));
		}
		return token;
	}
	public boolean search(String title) {
		ArrayList<String> token1, token2;
		String tmpTitle=title.toLowerCase();
		String tmpThisTitle= super.getTitle().toLowerCase();
		token1 = convertToTokens(tmpTitle);
        token2 = convertToTokens(tmpThisTitle);
        Collections.sort(token1);
        Collections.sort(token2);
        if (token1.equals(token2)) return true;
        return false;
		
	}
}
