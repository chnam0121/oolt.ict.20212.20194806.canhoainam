package hust.soict.globalict.aims.utils;

public class DateUtils {
	public static String[] monthArray= new String[] {"January","February","March","April","May","June","July","August","September","October","November","December"};
	public static boolean checkValidDate(int dayT,int monthT,int yearT) {
		if(dayT>31|| dayT<1 || monthT>12|| monthT<1 || yearT<0) return false;
		switch(monthT) {
		case 4:
		case 6:
		case 9:
		case 11:
			if(dayT>30) return false;
			break;
		case 2:
			if(dayT>29) return false;
			if(yearT%4!=0 || (yearT%100==0 && yearT%400!=0)) {
				if(dayT>28) return false;
			}
			break;
		}
		return true;
	}
	public static int dayReader(String date) {
		int i;
		int day;
		i=date.indexOf('/');
		try {
		    day = Integer.parseInt(date.substring(0, i));
		    return day;
		} catch (NumberFormatException e) {
		    return 0;
		}
	}
	public static int monthReader(String date) {
		int i,j;
		int month;
		i=date.indexOf('/');
		j=date.lastIndexOf('/');
		try {
		    month = Integer.parseInt(date.substring(i+1, j));
		    return month;
		} catch (NumberFormatException e) {
		    return 0;
		}
	}
	public static int yearReader(String date) {
		int i;
		int year;
		i=date.lastIndexOf('/');
		try {
		    year = Integer.parseInt(date.substring(i+1));
		    return year;
		} catch (NumberFormatException e) {
		    return 0;
		}
	}
	public static int compareDate(MyDate date1, MyDate date2) {
		if(date1.getYear()>date2.getYear()) return 1;
		if(date1.getYear()<date2.getYear()) return -1;
		if(date1.getMonth()<date2.getMonth()) return -1;
		if(date1.getMonth()>date2.getMonth()) return 1;
		if(date1.getDay()<date2.getDay()) return -1;
		if(date1.getDay()>date2.getDay()) return 1;
		return 0;
		
	}
	static void setDate(MyDate tmp,MyDate date1) {
		tmp.setYear(date1.getYear());
		tmp.setMonth(date1.getMonth());
		tmp.setDay(date1.getDay());
	}
	public static void wrapDate(MyDate date1,MyDate date2) {
		MyDate tmp=new MyDate(date1.getDay(),date1.getMonth(),date1.getYear());
		MyDate tmp1=new MyDate(1,1,1);
		setDate(date1, tmp1);
		setDate(date1, date2);
		setDate(date2, tmp1);
		setDate(date2, tmp);
	}
	public static void arrangeDate(MyDate[] dateList,int size) {
		int i,j;
		for(i=0;i<size;i++) {
			for(j=i+1;j<size;j++) {
				if(compareDate(dateList[j], dateList[i])==-1) {
					wrapDate(dateList[i], dateList[j]);
				}
			}
		}
	}
	
}
