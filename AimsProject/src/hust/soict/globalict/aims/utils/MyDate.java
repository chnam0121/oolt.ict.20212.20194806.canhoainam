package hust.soict.globalict.aims.utils;

import java.util.Scanner;
import java.text.DecimalFormat;
public class MyDate {
	private int day;
	private int month;
	private int year;
	private String dayString;
	private String monthString;
	private String yearString;
	public MyDate() {
	}
	public MyDate(int day, int month, int year) {
		this.day = day;
		this.month = month;
		this.year = year;
	}
	public MyDate(String date) {
		//form: day/month/year
		int  dayT=0, monthT=0, yearT=0;
		dayT=DateUtils.dayReader(date);
		monthT=DateUtils.monthReader(date);
		yearT=DateUtils.yearReader(date);
		boolean check=DateUtils.checkValidDate(dayT, monthT, yearT);
		if(check==false) System.out.println("The date is not valid");
		else {
			this.day=dayT;
			this.month=monthT;
			this.year=yearT;
		}
	}
	public int getDay() {
		return day;
	}
	public void setDay(int day) {
		if (DateUtils.checkValidDate(day, this.month, this.year)==false) {
			System.out.println("The date is not valid");
			return;
		}
		this.day = day;
	}
	public int getMonth() {
		return month;
	}
	public void setMonth(int month) {
		if (DateUtils.checkValidDate(this.day, month, this.year)==false) {
			System.out.println("The date is not valid");
			return;
		}
		this.month = month;
	}
	public int getYear() {
		return year;
	}
	public void setYear(int year) {
		if (DateUtils.checkValidDate(this.day, this.month, year)==false) {
			System.out.println("The date is not valid");
			return;
		}
		this.year = year;
	}
	
	public void setDay(String dayString) {
		this.dayString = dayString;
	}
	public void setMonth(String monthString) {
		this.monthString = monthString;
	}
	public void setYear(String yearString) {
		this.yearString = yearString;
	}
	
	/*public String getDayString() {
		return dayString;
	}
	public String getMonthString() {
		return monthString;
	}
	public String getYearString() {
		return yearString;
	}*/
	public void accept() {
		Scanner sc= new Scanner(System.in);
		int  dayT=0, monthT=0, yearT=0;
		boolean check;
		do {
		System.out.print("Enter current date (day/month/year): ");
		String date= sc.next();
		dayT=DateUtils.dayReader(date);
		monthT=DateUtils.monthReader(date);
		yearT=DateUtils.yearReader(date);
		check=DateUtils.checkValidDate(dayT, monthT, yearT);
		if(check==false) System.out.println("The date is not valid");
		}while(check==false);
		this.day=dayT;
		this.month=monthT;
		this.year=yearT;
	}
	 public void print() {
		 String date = DateUtils.monthArray[month-1]+" "+day;
		 if(day>10 && day<20) date +="th ";
		 else {
		 switch(day%10) {
		 case 1: date += "st ";break;
		 case 2: date += "nd ";break;
		 case 3: date += "rd ";break;
		 default: date +="th ";break;
		 }
		 }
		 date+=year;
		 System.out.println(date);
	 }
	 public String formatter(String form) {
		 DecimalFormat dm = new DecimalFormat("00");
		 DecimalFormat y = new DecimalFormat("0000");
		 char currentChar;
		 int numberOfChar;
		 String date="";
		 int i=0;
		 while(i<form.length()) {
			 currentChar=form.charAt(i);
			 numberOfChar=0;
			 do {
				 //System.out.println(form.length()+" "+i);
				 i++;
				 numberOfChar++;
			 } while(i<form.length() && form.charAt(i)==form.charAt(i-1));
			 //System.out.println("- "+numberOfChar+" -");
			 switch (currentChar) {
			case 'd':
				switch (numberOfChar) {
				case 1:
					date+=day;
					break;
				case 2:
					date+= dm.format(day);
					break;
				default:
					date+= dayString;
					break;
				}
				break;
			case 'M':
				switch (numberOfChar) {
				case 1:
					date+=month;
					break;
				case 2:
					date+=dm.format(month);
					break;
				case 3:
					date+=DateUtils.monthArray[month-1].substring(0, 3);
					break;
				default:
					date+=DateUtils.monthArray[month-1];
					break;
				}
				break;
			case 'y':
				switch (numberOfChar) {
				case 1:
				case 3:
					date+=year;
					break;
				case 2:
					date+=dm.format(year%100);
					break;
				case 4:
					date+=y.format(year);
					break;
				default:
					date+=yearString;
					break;
				}
				break;
			default:
				for(int j=0;j<numberOfChar;j++) {
					date+=currentChar;
				}
				break;
			}
		 }
		 return date;
	 }
	 public void print(String format) {
		 String strDate=formatter(format);
		 System.out.println(strDate);
	 }
}
