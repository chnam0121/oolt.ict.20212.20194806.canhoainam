package hust.soict.globalict.aims.order;
import java.util.ArrayList;
import java.util.List;

import java.lang.Math;
import hust.soict.globalict.aims.media.*;
import hust.soict.globalict.aims.utils.MyDate;
public class Order {
	public static final int MAX_ORDER_ITEMS= 10;
	public static final int MAX_LIMITED_ORDERS =5;
	public static final int	MEDIA_DVD=1;
	public static final int	MEDIA_CD=2;
	public static final int	MEDIA_BOOK=3;
	private MyDate dateOrdered;
	private static int nbOrders=0;
	private int luckyItem=-1;
	private List<Media> orderedItems = new ArrayList<Media>();
	//Constructor
	private Order() {
	}
	//Create an Oder
	public static Order createOrder() {
		if(nbOrders+1>MAX_LIMITED_ORDERS) {
			System.out.println("The current number of orders is over this limited number!");
			return null;
		}else {
			nbOrders++;
			return new Order();
		}
	}
	//Getter and Setter
	public String getDateOrdered() {
		return dateOrdered.formatter("dd/MM/yyyy");
	}
	public void setDateOrdered(MyDate dateOrdered) {
		this.dateOrdered = dateOrdered;
	}
	/*/create Media item
	public Media createMedia(int type) {
		if(type==MEDIA_DVD) {
			return new DigitalVideoDisc().inputInformation();
		}else if(type == MEDIA_CD) {
			return new CompactDisc().inputInformation();
		}else if(type== MEDIA_BOOK) {
			return new Book().inputInformation();
		}else return null;
	}*/
	//add and remove Media
	public void addMedia(Media media) {
		if(orderedItems.size()+1>MAX_ORDER_ITEMS) {System.out.println("The order is almost full");return;}
		if(orderedItems.contains(media)) {System.out.println("The item already exists in this order");return;}
		orderedItems.add(media);
		System.out.println("The item has been added");
	}
	public void removeMedia(int id) {
			if(id<0 || id>=orderedItems.size()) {
			System.out.println("Error");
			return;
		}
		orderedItems.remove(id);
		System.out.println("The disc has been removed");
	}
	
	// get Lucky Item
	public Media getALuckyItem() {
		int luckyItemIndex =(int)(Math.random()*orderedItems.size());
		luckyItem=luckyItemIndex;
		return orderedItems.get(luckyItemIndex);
	} 
	
	
	//Calculate total cost
	public double totalCost() {
		int i;
		double sum=0.0;
		for(i=0;i<orderedItems.size();i++) {
			if(i==luckyItem) continue;
			sum+=orderedItems.get(i).getCost();
		}
		return sum;
	}
	
	//Prints method
	public void showOrderInformation() {
		System.out.println("*********************Order************************");
		System.out.println("Date: "+getDateOrdered());
		System.out.println("Ordered Items:");
		int i;
		for(i=0;i<orderedItems.size();i++) {
			System.out.print((i)+". ");
			System.out.println(orderedItems.get(i));
		}
		System.out.printf("Total cost: %.3f\n",totalCost());
		System.out.println("**************************************************");
	}
	
}
