package hust.soict.globalict.test.utils;
import java.util.Scanner;

import hust.soict.globalict.aims.utils.*;
public class DateTest {
	public static final int MAX_LIMITED_DATES=5; 
	public static void menu() {
		System.out.println("__________________MENU__________________");
		System.out.println("1. Add a date!");
		System.out.println("2. Update name of day of current date");
		System.out.println("3. Update name of month of current date");
		System.out.println("4. Update name of year of current date");
		System.out.println("5. Print current date in form");
		System.out.println("6. Print list of dates");
		System.out.println("7. Sort dates");
		System.out.println("8. Exit");
		System.out.println("________________________________________");
	}
	public static void main(String[] args) {
		int numOfDate=0;
		MyDate[] myDate = new MyDate[MAX_LIMITED_DATES];
		int choice;
		String dayT,monthT,yearT;
		String form;
		Scanner sc= new Scanner(System.in);
		do {
			menu();
			System.out.print("-> Your choice: ");
			choice= sc.nextInt();
			switch(choice) {
			case 1:
				if(numOfDate+1>MAX_LIMITED_DATES) {
					System.out.println("The number of dates is over limition.");
					break;
				}
				numOfDate++;
				myDate[numOfDate-1]=new MyDate();
				myDate[numOfDate-1].accept();
				break;
			case 2:
				System.out.print("Enter the name of current day: ");
				sc.nextLine();
				dayT=sc.nextLine();
				myDate[numOfDate-1].setDay(dayT);
				break;
			case 3:
				System.out.print("Enter the name of current the month: ");
				monthT=sc.next();
				myDate[numOfDate-1].setMonth(monthT);
				break;
			case 4:
				System.out.print("Enter the name of current the year: ");
				sc.nextLine();
				yearT=sc.nextLine();
				myDate[numOfDate-1].setYear(yearT);
				break;
			case 5:
				System.out.print("Enter form of the date: ");
				sc.nextLine();
				form=sc.nextLine();
				System.out.print("The current date is: ");
				myDate[numOfDate-1].print(form);
				break;
			case 6:
				int i;
				System.out.println("List date:");
				for(i=0;i<numOfDate;i++) {
					System.out.print(i+1+". ");
					myDate[i].print();
				}
				break;
			case 7:
				DateUtils.arrangeDate(myDate,numOfDate);
				System.out.println("Successful!");
				break;
			case 8: break;
			default:
				System.out.println("Error!Please Enter again!");
			}
		}while(choice!=8);
	}

}
