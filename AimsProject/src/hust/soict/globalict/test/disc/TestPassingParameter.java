package hust.soict.globalict.test.disc;
import hust.soict.globalict.aims.media.DigitalVideoDisc;
public class TestPassingParameter {
	public static void main(String[] args) {
		DigitalVideoDisc jungleDVD = new DigitalVideoDisc("DVD01","Jungle");
		DigitalVideoDisc cinderellaDVD = new DigitalVideoDisc("DVD02","Cinderella");
		swap(jungleDVD,cinderellaDVD);
		System.out.println("jungle dvd title: "+jungleDVD.getTitle());
		System.out.println("cinderella dvd title: "+cinderellaDVD.getTitle());
		
		changeTitle(jungleDVD,cinderellaDVD.getTitle());
		System.out.println("jungle dvd title: "+jungleDVD.getTitle());
	}
	public static void swap(Object o1, Object o2) {
		Object tmpObject=o1;
		o1=o2;
		o2=tmpObject;
	}
	public static void changeTitle(DigitalVideoDisc dvd, String title) {
		String oldTitle = dvd.getTitle();
		int idString = dvd.getId();
		//dvd.setTitle(title);
		//dvd = new DigitalVideoDisc(idString,oldTitle);
	}
}
