package hust.soict.globalict.test;
import hust.soict.globalict.aims.media.Book;
import hust.soict.globalict.aims.media.factory.BookCreation;
import java.util.Scanner;
public class BookTest {
	public static void showMenu() {
		System.out.println("----------BOOK TEST----------");
		System.out.println("1. Enter new book.");
		System.out.println("2. Set/Update content of the book");
		System.out.println("3. Display Book information");
		System.out.println("0. Exit");
		System.out.println("-----------------------------");
	}
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		int choice;
		Book book=null;
		String content;
		Scanner scanner = new Scanner(System.in);
		do {
			showMenu();
			System.out.print("-> Your choice: ");
			choice = scanner.nextInt();
			switch (choice) {
			case 1:
				book = (Book)(new BookCreation().createMediaFromConsole());
				break;
			case 2:
				System.out.println("Enter content below: ");
				 scanner.nextLine();
				content = scanner.nextLine();
				book.setContent(content);
				break;
			case 3:
				System.out.println(book);
				break;
			case 0:
				break;
			default:
				System.out.println("Error!");
				break;
			}
		}while(choice!=0);
	}

}
